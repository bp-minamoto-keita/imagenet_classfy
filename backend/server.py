import base64
from logging import getLogger
import os
import shutil
# import matplotlib.pyplot as plt
from io import BytesIO
from typing import List, Dict

from PIL.Image import Image
from fastapi import FastAPI, BackgroundTasks, File, UploadFile, Request
from fastapi.responses import FileResponse
from src import vgg16_classfy
IMAGE_SIZE = (224, 224)
IMAGE_DIR_PATH = 'data/images/'
# plt.switch_backend('Agg')
logger = getLogger(__name__)
app = FastAPI()


@app.get('/')
async def get_file_names():
    """
    アップロード済み画像ファイル名の一覧を取得

    Returns:

    """
    file_names = os.listdir(IMAGE_DIR_PATH)
    return file_names


@app.get('/image/')
async def get_image(filename: str):
    """
    filename名の画像ファイルを取得

    Args:
        filename:

    Returns:

    """
    image_file_path = f'{IMAGE_DIR_PATH}{filename}'
    return FileResponse(
        image_file_path,
        filename=image_file_path)


@app.post('/image/')
async def upload(files: List[UploadFile] = File(...)):
    """
    アップロードされたファイルを保存する

    Args:
        files:

    Returns:

    """
    for image_file in files:
        image_file_path = f'{IMAGE_DIR_PATH}{image_file.filename}'
        with open(image_file_path, "wb") as buffer:
            shutil.copyfileobj(image_file.file, buffer)
    return "Success upload"


@app.get('/download_link/')
async def download(filename: str):
    """
    filename名の画像ファイルのダウンロードを取得

    Args:
        filename:

    Returns:

    """
    image_file_path = f'{IMAGE_DIR_PATH}{filename}'
    img = Image.open(image_file_path)
    r_bytes = BytesIO(img)
    b64 = base64.b64encode(r_bytes.getvalue()).decode()
    html = f'<a href="data:application/jpg;base64,{b64}" download="test.jpg">Download image file</a>'
    return html


@app.delete('/image/')
async def delete(filename: str):
    """
    filename名の画像ファイルを削除

    Args:
        filename:

    Returns:

    """
    image_file_path = f'{IMAGE_DIR_PATH}{filename}'
    os.remove(image_file_path)
    return f"Success delete {filename}"


@app.put('/image/')
async def update(filename: str, new_filename: str):
    """
    filename名の画像ファイルを名前変更

    Args:
        filename:
        new_filename:

    Returns:

    """
    os.rename(f'{IMAGE_DIR_PATH}{filename}', f'{IMAGE_DIR_PATH}{new_filename}')
    return f"Success update {filename}"


@app.get('/image/classifier/')
async def classification(filename):
    """
    filename名の画像ファイルを分類

    Args:
        filename:

    Returns:

    """
    class_label = vgg16_classfy.predict_label(f'{IMAGE_DIR_PATH}{filename}')
    logger.info(class_label)
    return f"分類結果: {class_label}"
