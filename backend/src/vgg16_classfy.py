"""
vgg16の学習済みモデルにより画像の分類を行う
"""
import argparse
import json
import torch
import torchvision
from PIL import Image
from torchvision import transforms

preprocess = transforms.Compose([
    transforms.Resize(224),
    transforms.CenterCrop(224),
    transforms.ToTensor(),
    transforms.Normalize(
        mean=[0.485, 0.456, 0.406],
        std=[0.229, 0.224, 0.225]
    )
])


def get_device(gpu_id=-1):
    if gpu_id >= 0 and torch.cuda.is_available():
        return torch.device("cuda", gpu_id)
    else:
        return torch.device("cpu")


def get_image_data(image_path):
    """
    image_pathの画像ファイルをモデルに入力できる形式に変換

    Args:
        image_path:画像ファイルのpath

    Returns:
        image_tensor
    """
    # image_tensor = cv2.imread(image_path)
    # image_tensor = cv2.cvtColor(image_tensor, cv2.COLOR_BGR2RGB)
    image_tensor = Image.open(image_path)
    image_tensor = preprocess(image_tensor)
    image_tensor = image_tensor[None]
    return image_tensor


def predict_label(image_path):
    """
    vgg16の学習済みモデルにより画像の分類を行う

    Args:
        image_path:分類する画像ファイルのpath

    Returns:
        予測結果のラベル
    """
    device = get_device(gpu_id=-1)
    data = get_image_data(image_path)
    vgg16 = torchvision.models.vgg16(pretrained=True)
    vgg16.eval()
    prediction = vgg16(data.to(device))
    prediction_idx = torch.argmax(prediction[0]).item()
    with open('data/imagenet_class_index.json') as f:
        labels = json.load(f)
    result = labels[str(prediction_idx)][1]
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--image_path",
        help="image file path",
        default="data/images/sample.jpg",
        type=str)
    args = parser.parse_args()

    import warnings
    warnings.filterwarnings('ignore')
    print(f"Input:{args.image_path}")
    print(f"Result:{predict_label(args.image_path)}")
