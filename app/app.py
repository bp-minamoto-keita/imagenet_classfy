import logging
import os
import streamlit as st
import httpx
import base64
from io import BytesIO

from PIL.Image import Image
from httpx import Timeout

BACKEND_ORIGIN = os.environ.get('BACKEND_ORIGIN', 'http://localhost:80')


def make_image_donwload_link(image_file_response, filename):
    """
    ダウンロードリンクの作成．

    TODO:
        ダウンロード時のファイル名の指定ができない

    Args:
        image_file_response:

    Returns:

    """
    r_bytes = BytesIO(image_file_response.content)
    b64 = base64.b64encode(r_bytes.getvalue()).decode()
    # html = f'<img  src="{data_url_scheme}" download="test.png">'
    data_url_scheme = f'data:image/*;base64,{b64}'
    html = f'<p><a download="{filename}" href="{data_url_scheme}">Download image file</a></p>'
    # html = f'<p><a download="test.png" href={data_url_scheme}>Download image file</a></p>'
    return html


def main():
    st.title('ImageNet Classification')
    r = httpx.get(f'{BACKEND_ORIGIN}/')
    filenames = r.json()
    target_file = st.selectbox('アップロード済み画像', filenames)
    if target_file:
        params = {"filename": target_file}
        r = httpx.get(
            f'{BACKEND_ORIGIN}/image/',
            params=params)
        st.image(r.content, caption=target_file, use_column_width=True)

    image_files = st.file_uploader('Upload',
                                   type=['png', 'jpg', 'jpeg'],
                                   accept_multiple_files=True
                                   )
    if len(image_files) > 0:
        files = [('files', file) for file in image_files]
        if st.button('Upload'):
            r = httpx.post(f'{BACKEND_ORIGIN}/image/', files=files)
            st.success(r.json())

    if st.button('Download'):
        params = {"filename": target_file}
        r = httpx.get(
            f'{BACKEND_ORIGIN}/image/',
            params=params)
        download_link = make_image_donwload_link(r, target_file)
        st.markdown(download_link, unsafe_allow_html=True)

    if st.button('Delete'):
        params = {"filename": target_file}
        r = httpx.delete(f'{BACKEND_ORIGIN}/image/', params=params)
        st.success(r.json())

    new_filename = st.text_input('変更後ファイル名', 'new_file.jpg')
    if st.button('Update'):
        params = {"filename": target_file, "new_filename": new_filename}
        r = httpx.put(f'{BACKEND_ORIGIN}/image/', params=params)
        st.success(r.json())

    if st.button('Classification'):
        params = {"filename": target_file}
        r = httpx.get(
            f'{BACKEND_ORIGIN}/image/classifier/',
            params=params,
            timeout=Timeout(
                timeout=5,
                connect=5,
                read=5 * 60,
                write=5))
        st.success(r.json())


if __name__ == '__main__':
    main()
